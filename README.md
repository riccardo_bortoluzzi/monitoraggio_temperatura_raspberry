# Monitoraggio utilizzo Raspberry

## Introduzione

Questo progetto nasce con l'idea di creare un software di monitoraggio continuo dell'utilizzo di una Raspberry Pi.

Il monitoraggio prevede la registrazione in un database di alcuni parametri del dispositivo e la generazione dei rispettivi grafici dell'ultima giornata, dell'ultima settiamna, dell'ultimo mese (inteso come 30 giorni) e dell'ultimo anno (inteso come 365 giorni).

I valori registrati sono:
* Temperatura della CPU (° C) (*)
* Frequenza utilizzo CPU (MHz)
* Percentuale di carico nella CPU (*)
* RAM disponibile (MB)
* Percentuale utilizzo RAM (*)

*: valori attualmente mostrati nei grafici insieme alle rispettive medie.

Le funzioni di recupero dei rispettivi valori provengono da uno script utilizzato dall'applicazione RaspController di Ettore Gallina recuperabile al seguente link:
[https://www.gallinaettore.com/_dataserver/raspcontroller/monitoring_v12.py](https://www.gallinaettore.com/_dataserver/raspcontroller/monitoring_v12.py)

## Prerequisiti

Per poter eseguire lo script è necessario aver installato Python3, inoltre sarebbero da installare le seguenti librerie di Python:
```
pip install psutil
pip install matplotlib
```

## Esecuzione
Lo script si esegue lanciando il file 
```
main_salvataggio_valori.py
```

Si potrebbe schedulare l'esecuzione di questo script ad intervalli regolari, ad esempio tramite l'utilizzo di `crontab`. Una simulazione presente nel database `database/monitoraggio_raspberry_test_2.db` di una schedulazione ogni 5 minuti per un anno porterebbe la dimensione del file a una decina di MB.

Lo script si occuperà di:
1. Cancellare le registrazioni più vecchie di 400 giorni;
2. Salvare una nuova registrazione nel database;
3. Creare i grafici del monitoraggio per i vari periodi di tempo sopra descritti all'interno della cartella `immagini`;
4. Creare la pagina `pagina_monitoraggio.html` con all'interno i valori dell'ultima registrazione e i grafici come immagini codificate in base64.

## Cartelle

### database
Contiene i vari database:
* `monitoraggio_raspberry.db` : database utilizzato per registrare i vari valori.
* `monitoraggio_raspberry_pulito.db` : file del database da copiare e rinominare nel caso si volesse ripristinare il database utilizzato.
* `monitoraggio_raspberry_test_1.db` : database di test con una simulazione di circa 6 mesi di registrazioni ogni 10 minuti con dati casuali.
* `monitoraggio_raspberry_test_2.db` : database di test con una simulazione di circa 12 mesi di registrazioni ogni 5 minuti con dati calcolati tramite funzioni esponenziali e arcotangenti.

### immagini
Contiene le varie immagini dei grafici degli andamenti dei valori.
Il nome dei file è strutturato nel seguente modo:

`{tipo_valore}_{periodo}.png`

Per evitare di salvare ogni volta i grafici, si può impostare la costante `SALVATAGGIO_IMMAGINI_GRAFICI` all'interno del file `main_salvataggio_valori.py` a `False`. Questa opzione non disabiliterà comunque la creazione della pagina html con i grafici aggiornati.

### src
Contiene i sorgenti python del progetto.

### template

Contiene il file di template per generare la pagina html con le informazioni dell'ultimo rilevamento e i grafici dello storico.

### test
Cartella contenente alcuni script utilizzati in fase di test o per popolare i database delle simulazioni.



