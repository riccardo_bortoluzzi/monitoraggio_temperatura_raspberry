#script per testre i grafici

import sys, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from src.grafico import Grafico

if __name__ == '__main__':
	nuovo_grafico = Grafico(sigla='anno')
	nuovo_grafico.crea_serie_grafici(flag_salva_grafico=True)