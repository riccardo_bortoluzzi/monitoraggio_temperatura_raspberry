#!/usr/bin/python3

#script di main per la creazione dei grafici

import sys, os
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from src.grafico import DIZ_INTERVALLI, Grafico

def main():
	#per prima cosa mostro i valori attuali
	#oggetto_valori = ValoriDB.cre_da_valori_attuali()
	#print(oggetto_valori)
	#adesso creo i grafici per tutti i periodi
	diz_base_64 = {}
	for k in DIZ_INTERVALLI.keys():
		nuovo_grafico = Grafico(sigla=k)
		diz_base_64[k] = nuovo_grafico.crea_serie_grafici(flag_salva_grafico=True)

if __name__=='__main__':
	main()