#!.venv/bin/python3

#script di main per il savlataggio dei valori

from src.valoriDB import ValoriDB
import src.database as db
from src.valoriDB import ValoriDB
#from src.grafico import crea_grafici_pagina




def main():
	#elimino i vecchi record
	db.elimina_vecchi_record()
	#creo un nuovo oggetto di valori e lo inserisco
	nuovo_oggetto_valori = ValoriDB.crea_da_valori_attuali()
	#nuovo_oggetto_valori = ValoriDB.crea_da_valori_casuali()
	nuovo_oggetto_valori.inserisci_in_db()
	#creo i vari grafici
	#crea_grafici_pagina()
	

if __name__ == '__main__':
	main()
