import subprocess
import psutil

def get_cpu_temperature():
	output = subprocess.check_output("/usr/bin/cat /sys/class/thermal/thermal_zone0/temp", shell=True)
	decoded_output = output.decode("utf-8")
	return float(decoded_output)/1000

def read_freq_from_line(line):
    part = line.split(":")
    return float(part[1].strip().replace(",", "."))

def get_cpu_min_max_freqs():
	try:
		output = subprocess.check_output("/usr/bin/lscpu", shell=True)
		decoded_output = output.decode("utf-8")
		freq_min = 0
		freq_max = 0
		for line in decoded_output.split("\n"):
			if "CPU max" in line:
				freq_max = read_freq_from_line(line)
			elif "CPU min" in line:
				freq_min = read_freq_from_line(line)
		return (freq_min, freq_max)
	except:
		return (0, 0)


def get_cpu_cur_freq():
	try:
		output = subprocess.check_output("/usr/bin/vcgencmd measure_clock arm | cut -d '=' -f 2", shell=True)
		decoded_output = output.decode("utf-8")
		curr_freq = int(decoded_output)/1000000
		return curr_freq
	except:
		return 0

 
def get_cpu_count():
	try:
		return psutil.cpu_count()
	except:
		return 1