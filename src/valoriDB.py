#script per la classe con i valori del database

from __future__ import annotations

from datetime import datetime
import psutil, random

import src.funzioni as funzioni
import src.database as db

B_TO_MB = float(2**20)

class ValoriDB:
	data : datetime
	temperatura : float
	frequenza_attuale : float
	percentuale_cpu : float
	ram_disponibile : float
	percentuale_utilizzo_ram : float

	def inserisci_in_db(self):
		#metodo per inserire il record nel db
		#mi calcolo il dizionario
		dizionario_valori = dict( 
			zip( 
				db.LISTA_CAMPI, 
				[
					self.data.timestamp(),
					self.temperatura,
					self.frequenza_attuale,
					self.percentuale_cpu,
					self.ram_disponibile,
					self.percentuale_utilizzo_ram
				]
			) 
		)
		db.inserisci_riga(diz_campi=dizionario_valori)

	@classmethod
	def crea_da_tupla(cls, tupla:tuple) -> ValoriDB:
		#metodo per popolare i campi dalla tupla presa dal db e ritornare un nuovo oggetto
		nuovo_ogg_valori = ValoriDB()
		(
			float_data,
			nuovo_ogg_valori.temperatura,
			nuovo_ogg_valori.frequenza_attuale,
			nuovo_ogg_valori.percentuale_cpu,
			nuovo_ogg_valori.ram_disponibile,
			nuovo_ogg_valori.percentuale_utilizzo_ram
		) = tupla
		nuovo_ogg_valori.data = datetime.fromtimestamp(float_data)
		return nuovo_ogg_valori

	#metodo per popolare i valori attuali
	@classmethod
	def crea_da_valori_attuali(cls) -> ValoriDB:
		nuovo_ogg_valori = ValoriDB()
		nuovo_ogg_valori.data = datetime.now()
		#temperatura
		nuovo_ogg_valori.temperatura = round(funzioni.get_cpu_temperature(), 2)
		#cpu
		nuovo_ogg_valori.percentuale_cpu = round(psutil.cpu_percent(0.1, False), 2)
		try:
			freqs = psutil.cpu_freq()
			nuovo_ogg_valori.frequenza_attuale= round(freqs.current, 2)
		except:
			nuovo_ogg_valori.frequenza_attuale= round( funzioni.get_cpu_cur_freq(), 2)
		#ram
		ram = psutil.virtual_memory()
		
		nuovo_ogg_valori.ram_disponibile = round(ram.available / B_TO_MB, 2)
		nuovo_ogg_valori.percentuale_utilizzo_ram = round(ram.percent, 2)
		return nuovo_ogg_valori

	@classmethod
	def crea_da_valori_casuali(cls) -> ValoriDB:
		#metodo per creare una riga di valori casuali
		nuovo_ogg_valori = ValoriDB()
		nuovo_ogg_valori.data = datetime.now()
		#temperatura
		nuovo_ogg_valori.temperatura = round(random.random() * 100, 2)
		nuovo_ogg_valori.percentuale_cpu = round(random.random() * 100, 2)
		nuovo_ogg_valori.frequenza_attuale= round(random.random() * 1000, 2)
		nuovo_ogg_valori.ram_disponibile = round(random.random()*1000, 2)
		nuovo_ogg_valori.percentuale_utilizzo_ram = round( random.random()*100, 2)
		return nuovo_ogg_valori

	def __init__(self) -> None:
		#viene popolato dalle rispettive funzioni di popolamento
		pass

	def __str__(self):
		#metodo per convertire l'oggetto in stringa
		stringa = f'Data misurazione: {self.data.strftime("%d/%m/%Y %H:%M:%S")}\n'
		stringa += f'Temperatura attuale: {self.temperatura}°C\n'
		stringa += f'Frequenza CPU attuale: {self.frequenza_attuale} MHz\n'
		stringa += f'Percentuale utilizzo CPU: {self.percentuale_cpu}%\n'
		stringa += f'Ram disponibile: {self.ram_disponibile} MB\n'
		stringa += f'Percentuale Ram utilizzata: {self.percentuale_utilizzo_ram}%'
		return stringa

	






