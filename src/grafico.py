#script che contiene la classe per gestire il grafico

from typing import Dict, List
from datetime import datetime, timedelta
import os, io, base64
import matplotlib.pyplot as plt
import matplotlib.dates as mdates

from src.valoriDB import ValoriDB
import src.database as db
import src.costanti as costanti

CHIAVE_RAM = 'RAM'
CHIAVE_CPU = 'CPU'
CHIAVE_TEMPERATURA = 'TEMP'

CHIAVE_GIORNO = 'giorno'
CHIAVE_SETTIMANA = 'settimana'
CHIAVE_MESE = 'mese'
CHIAVE_ANNO = 'anno'

DIZ_INTERVALLI = {
	CHIAVE_GIORNO : 1,
	CHIAVE_SETTIMANA : 7,
	CHIAVE_MESE : 30,
	CHIAVE_ANNO : 365
}

COLOR_RED = 'tab:red'
COLOR_BLUE = 'tab:blue'


FORMATO_DATA = '%d/%m/%Y - %H:%M:%S'

CARTELLA_IMMAGINI = os.path.join( costanti.CARTELLA_ATTUALE, 'immagini' )

__PATH_PAGINA_TEMPLATE = os.path.join( costanti.CARTELLA_ATTUALE, 'template', 'template_pagina.html' )
PATH_PAGINA_CREATA = os.path.join( costanti.CARTELLA_ATTUALE, 'pagina_monitoraggio.html' )

__SALVATAGGIO_IMMAGINI_GRAFICI = True

class Grafico:
	lista_valori : List[ValoriDB]
	sigla_intervallo : str

	def __init__(self, sigla:str) -> None:
		#mi recupero la data attuale e il delta
		self.sigla_intervallo = sigla
		data_attuale = datetime.now()
		delta = timedelta(days=DIZ_INTERVALLI[self.sigla_intervallo])
		#recupero tutti i valori dal db intanto
		self.lista_valori = [ ValoriDB.crea_da_tupla(tupla=i) for i in db.ricerca_righe_valori(data_inizio=data_attuale-delta, data_fine=data_attuale) ]
		#genero i grafici
		#self.crea_serie_grafici()

	###################################################################################################################################################################
	#metodi per creare le figure

	def crea_serie_grafici(self, flag_salva_grafico:bool) -> Dict[str, str]:
		#metodo per generare tutti i grafici
		diz_base_64 = {}
		diz_base_64[CHIAVE_TEMPERATURA] = self.crea_grafico_temperatura(flag_salva_grafico=flag_salva_grafico)
		diz_base_64[CHIAVE_CPU] = self.crea_grafico_cpu(flag_salva_grafico=flag_salva_grafico)
		diz_base_64[CHIAVE_RAM] = self.crea_grafico_ram(flag_salva_grafico=flag_salva_grafico)
		return diz_base_64

	def crea_grafico_temperatura(self, flag_salva_grafico:bool) -> str:
		#metodo per creare il grafico della temperatura
		#fig = plt.figure(figsize=(1080, 1080))
		#ax = fig.add_subplot(111)
		fig, ax = plt.subplots(figsize=(10, 6))
		#calcolo anche la media
		media = sum( [ i.temperatura for i in self.lista_valori ] )/ ( len(self.lista_valori) if len(self.lista_valori) > 0 else 1 )
		ax.plot([v.data for v in self.lista_valori], [v.temperatura for v in self.lista_valori], color=COLOR_BLUE)
		ax.plot([v.data for v in self.lista_valori], [media for _ in self.lista_valori], color=COLOR_RED)
		ax.set(
			ylabel='Temperatura (°C)',
			title=f'Temperatura {self.sigla_intervallo}'
		)
		# Rotates and right-aligns the x labels so they don't crowd each other.
		for label in ax.get_xticklabels(which='major'):
			label.set(rotation=10, horizontalalignment='right')
		ax.xaxis.set_major_formatter(mdates.DateFormatter(FORMATO_DATA))
		ax.grid()
		if flag_salva_grafico:
			fig.savefig( os.path.join(CARTELLA_IMMAGINI, f'temperatura_{self.sigla_intervallo}.png') )
		#fig.show()
		#mi salvo l'immagine in un fake file per ricavarne il base64
		my_string_bytes = io.BytesIO()
		plt.savefig(my_string_bytes, format='png')
		my_string_bytes.seek(0)
		my_base64_png_data = base64.b64encode(my_string_bytes.read())
		return my_base64_png_data.decode('utf-8', errors='ignore')

	def crea_grafico_cpu(self, flag_salva_grafico:bool) -> str:
		#metodo per creare il grafico della cpu
		#fig = plt.figure(figsize=(1080, 1080))
		#ax1 = fig.add_subplot(111)
		fig, ax1 = plt.subplots(figsize=(10, 6))

		#calcolo anche la media
		media = sum( [ i.percentuale_cpu for i in self.lista_valori ] )/ ( len(self.lista_valori) if len(self.lista_valori) > 0 else 1 )

		#color = 'tab:red'
		#ax1.set_ylabel('Frequenza cpu (MHz)', color=color)
		#ax1.plot([v.data for v in self.lista_valori], [v.frequenza_attuale for v in self.lista_valori], color=color)
		#ax1.tick_params(axis='y', labelcolor=color)
		
		#ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
		#color = 'tab:blue'
		ax1.set_ylabel('Percentuale utilizzo (%)')#, color=COLOR_BLUE)  # we already handled the x-label with ax1
		ax1.plot([v.data for v in self.lista_valori], [v.percentuale_cpu for v in self.lista_valori], color=COLOR_BLUE)
		ax1.plot([v.data for v in self.lista_valori], [media for _ in self.lista_valori], color=COLOR_RED)
		#ax1.tick_params(axis='y', labelcolor=color)

		#fig.tight_layout()  # otherwise the right y-label is slightly clipped

		# Rotates and right-aligns the x labels so they don't crowd each other.
		for label in ax1.get_xticklabels(which='major'):
			label.set(rotation=10, horizontalalignment='right')
		
		ax1.set(
			title=f'Frequenza CPU {self.sigla_intervallo}'
		)
		ax1.xaxis.set_major_formatter(mdates.DateFormatter(FORMATO_DATA))
		ax1.grid()
		if flag_salva_grafico:
			fig.savefig( os.path.join(CARTELLA_IMMAGINI, f'frequenza_{self.sigla_intervallo}.png') )
		#fig.show()
		#mi salvo l'immagine in un fake file per ricavarne il base64
		my_string_bytes = io.BytesIO()
		plt.savefig(my_string_bytes, format='png')
		my_string_bytes.seek(0)
		my_base64_png_data = base64.b64encode(my_string_bytes.read())
		return my_base64_png_data.decode('utf-8', errors='ignore')

	def crea_grafico_ram(self, flag_salva_grafico:bool) -> str:
		#metodo per creare il grafico della ram
		#calcolo la media
		media = sum( [ i.percentuale_utilizzo_ram for i in self.lista_valori ] )/ ( len(self.lista_valori) if len(self.lista_valori) > 0 else 1 )

		#fig = plt.figure(figsize=(1080, 1080))
		#ax1 = fig.add_subplot(111)
		fig, ax1 = plt.subplots(figsize=(10, 6))

		#color = 'tab:red'
		#ax1.set_ylabel('Ram disponibile (MB)', color=color)
		#ax1.plot([v.data for v in self.lista_valori], [v.ram_disponibile for v in self.lista_valori], color=color)
		#ax1.tick_params(axis='y', labelcolor=color)
		
		#ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
		#color = 'tab:blue'
		ax1.set_ylabel('Percentuale utilizzo ram (%)')#, color=color)  # we already handled the x-label with ax1
		ax1.plot([v.data for v in self.lista_valori], [v.percentuale_utilizzo_ram for v in self.lista_valori], color=COLOR_BLUE)
		ax1.plot([v.data for v in self.lista_valori], [media for _ in self.lista_valori], color=COLOR_RED)
		#ax1.tick_params(axis='y', labelcolor=color)

		#fig.tight_layout()  # otherwise the right y-label is slightly clipped

		# Rotates and right-aligns the x labels so they don't crowd each other.
		for label in ax1.get_xticklabels(which='major'):
			label.set(rotation=10, horizontalalignment='right')
		
		ax1.set(
			title=f'RAM {self.sigla_intervallo}'
		)
		ax1.xaxis.set_major_formatter(mdates.DateFormatter(FORMATO_DATA))
		ax1.grid()
		if flag_salva_grafico:
			fig.savefig( os.path.join(CARTELLA_IMMAGINI, f'ram_{self.sigla_intervallo}.png') )
		#fig.show()
		#mi salvo l'immagine in un fake file per ricavarne il base64
		my_string_bytes = io.BytesIO()
		plt.savefig(my_string_bytes, format='png')
		my_string_bytes.seek(0)
		my_base64_png_data = base64.b64encode(my_string_bytes.read())
		return my_base64_png_data.decode('utf-8', errors='ignore')
		
def crea_grafici_pagina():
	#funzione per creare tutti i grafici necessari
	diz_base_64 = {}
	nuovo_oggetto_valori = ValoriDB.crea_da_valori_attuali()
	for k in DIZ_INTERVALLI.keys():
		nuovo_grafico = Grafico(sigla=k)
		diz_base_64[k] = nuovo_grafico.crea_serie_grafici(flag_salva_grafico=__SALVATAGGIO_IMMAGINI_GRAFICI)
	#creo la pagina html
	with open(__PATH_PAGINA_TEMPLATE, 'r') as f:
		contenuto_pagina = f.read()
	with open( PATH_PAGINA_CREATA, 'w' ) as f:
		f.write(
			contenuto_pagina.format(
				temperatura = nuovo_oggetto_valori.temperatura,
				frequenza_cpu = nuovo_oggetto_valori.frequenza_attuale,
				percentuale_cpu = nuovo_oggetto_valori.percentuale_cpu,
				ram_disponibile = nuovo_oggetto_valori.ram_disponibile,
				percentuale_ram = nuovo_oggetto_valori.percentuale_utilizzo_ram,
				orario= nuovo_oggetto_valori.data.strftime('%d/%m/%Y - %H:%M:%S'),

				temperatura_giorno = diz_base_64[CHIAVE_GIORNO][CHIAVE_TEMPERATURA],
				temperatura_settimana = diz_base_64[CHIAVE_SETTIMANA][CHIAVE_TEMPERATURA],
				temperatura_mese = diz_base_64[CHIAVE_MESE][CHIAVE_TEMPERATURA],
				temperatura_anno = diz_base_64[CHIAVE_ANNO][CHIAVE_TEMPERATURA],

				cpu_giorno = diz_base_64[CHIAVE_GIORNO][CHIAVE_CPU],
				cpu_settimana = diz_base_64[CHIAVE_SETTIMANA][CHIAVE_CPU],
				cpu_mese = diz_base_64[CHIAVE_MESE][CHIAVE_CPU],
				cpu_anno = diz_base_64[CHIAVE_ANNO][CHIAVE_CPU],

				ram_giorno = diz_base_64[CHIAVE_GIORNO][CHIAVE_RAM],
				ram_settimana = diz_base_64[CHIAVE_SETTIMANA][CHIAVE_RAM],
				ram_mese = diz_base_64[CHIAVE_MESE][CHIAVE_RAM],
				ram_anno = diz_base_64[CHIAVE_ANNO][CHIAVE_RAM]

			)
		)
	
		
