#script per gestire il databse 

import sqlite3, os
from typing import Any, Dict, Generator, List, Union
from datetime import datetime, timedelta

import src.costanti as costanti


PATH_DB = os.path.join(costanti.CARTELLA_ATTUALE, 'database', 'monitoraggio_raspberry.db' )

LISTA_CAMPI = [ 'data', 'temperatura', 'frequenza_attuale', 'percentuale_cpu', 'ram_disponibile', 'percentuale_utilizzo_ram' ]
NOME_TABELLA = 'valori_monitoraggio'

#metto le varie funzioni che possono servirmi

def ricerca_righe_valori(data_inizio:datetime, data_fine:datetime) -> Generator[tuple, None, None]:
	#funzione per ricercare dei record nel db e ritornare la lista con tutti i record trovati
	#mi creo la stringa per ricercare le cose
	stringa_ricerca = f" SELECT {', '.join(LISTA_CAMPI)} FROM {NOME_TABELLA} WHERE {LISTA_CAMPI[0]} between ? and ? order by {LISTA_CAMPI[0]}"
	#adesso posso finalmente eseguire la ricerca
	conn_db = sqlite3.connect(PATH_DB)
	cursore = conn_db.cursor()
	cursore.execute(stringa_ricerca, [ data_inizio.timestamp(), data_fine.timestamp() ])
	for i in cursore:
		yield i
	#chiudo tutto
	cursore.close()
	conn_db.close()

def inserisci_riga(diz_campi:Dict[str, Any]) -> None:
	#funzione per inserire una nuova riga al db
	nomi_campi = list(diz_campi.keys())
	valori_campi = [ diz_campi[i] for i in nomi_campi ]
	stringa_insert = f" INSERT INTO {NOME_TABELLA} ( {', '.join(nomi_campi)} ) VALUES ( {', '.join( [ '?' for _ in nomi_campi ] )} ) "
	conn_db = sqlite3.connect(PATH_DB)
	cursore = conn_db.cursor()
	cursore.execute(stringa_insert, valori_campi)
	conn_db.commit()
	#chiudo tutto e ritorno None
	cursore.close()
	conn_db.close()
	return None

def elimina_vecchi_record():
	#funzione per eliminare i record piu vecchi di 400 giorni
	delta = timedelta(days=400)
	data_inizio_ricerca = datetime.now() - delta
	stringa_eliminazione = f'DELETE FROM {NOME_TABELLA} WHERE {LISTA_CAMPI[0]} < ?'
	conn_db = sqlite3.connect(PATH_DB)
	cursore = conn_db.cursor()
	cursore.execute(stringa_eliminazione, [ data_inizio_ricerca.timestamp() ])
	conn_db.commit()
	#chiudo tutto e ritorno None
	cursore.close()
	conn_db.close()
	return None
	
